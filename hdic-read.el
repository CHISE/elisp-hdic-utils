;;; hdic-read.el --- Utility to read the HDIC database ; -*- coding: utf-8-mcs-er; -*-

;; Copyright (C) 2022, 2023 MORIOKA Tomohiko

;; Author: MORIOKA Tomohiko <tomo@kanji.zinbun.kyoto-u.ac.jp>
;; Keywords: HDIC, CHISE

;; This file is a part of HDIC-utils.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Code:

(require 'est-eval)

(defvar hdic-tsj-entries-file     "~/projects/hdic/HDIC/TSJ_entries.tsv")
(defvar hdic-tsj-definitions-file "~/projects/hdic/HDIC/TSJ_definitions.tsv")
(defvar hdic-syp-entries-file "~/projects/hdic/HDIC_chise/SYP.tsv")
(defvar hdic-ktb-entries-file "~/projects/hdic/HDIC/KTB.tsv")
(defvar hdic-ktb-ndl-file     "~/projects/hdic/HDIC/KTB_ndl.txt")

(defvar hdic-tsj-image-dir "~/projects/hdic/tsjimg/")
(defvar hdic-syp-image-dir "~/projects/hdic/sypimg/")
(defvar hdic-ktb-image-dir "~/projects/hdic/ktbimg/")


(defun ruimoku-encode-name-as-id (name)
  (let (ucs ret)
    (mapconcat (lambda (c)
		 (cond
		  ((eq c ?\ )
		   "_"
		   )
		  ((or (and (<= ?A c)(<= c ?Z))
		       (and (<= ?a c)(< c ?u))
		       (and (< ?u c)(< c ?z))
		       (and (<= ?0 c)(<= c ?9)))
		   (char-to-string c)
		   )
		  ((setq ucs (char-ucs c))
		   (format "u%04X" ucs)
		   )
		  ((setq ret (encode-char c '=ruimoku-v6))
		   (format "r%04X" ret)
		   )
		  ((setq ret (encode-char c '=jef-china3))
		   (format "j%04X" ret)
		   )
		  (t
		   (error "Unknown character %c" c)
                   ;; (format "m%08X" (char-id c))
		   )))
	       name
	       "")))

(defun hanzi-syllable-get-fanqie-initial (initial)
  (if (characterp initial)
      (setq initial (char-to-string initial)))
  (let (cobj id)
    (setq cobj (concord-decode-object '=initial-character initial 'hanzi-syllable))
    (unless cobj
      (setq id (intern (format "initial-%s" (ruimoku-encode-name-as-id initial))))
      (setq cobj (concord-make-object 'hanzi-syllable id))
      (concord-object-put cobj 'name (format "\u58F0母字「%s」" initial))
      (concord-object-put cobj '=initial-character initial))
    cobj))

(defun hanzi-syllable-get-fanqie-final (final)
  (if (characterp final)
      (setq final (char-to-string final)))
  (let (cobj id)
    (setq cobj (concord-decode-object '=final-character final 'hanzi-syllable))
    (unless cobj
      (setq id (intern (format "final-%s" (ruimoku-encode-name-as-id final))))
      (setq cobj (concord-make-object 'hanzi-syllable id))
      (concord-object-put cobj 'name (format "韻母字「%s」" final))
      (concord-object-put cobj '=final-character final))
    cobj))

(defun hanzi-syllable-get-fanqie (fanqie)
  (let (fanqie-cobj id)
    (setq fanqie-cobj (concord-decode-object '=fanqie fanqie 'hanzi-syllable))
    (unless fanqie-cobj
      (setq id (intern (format "fanqie-%s" (ruimoku-encode-name-as-id fanqie))))
      (setq fanqie-cobj (concord-make-object 'hanzi-syllable id))
      (concord-object-put fanqie-cobj '=fanqie fanqie)
      (concord-object-put fanqie-cobj 'name (format "(\u53CD\u5207)%s" fanqie))
      (concord-object-put fanqie-cobj '->initial-character
			  (list (hanzi-syllable-get-fanqie-initial (aref fanqie 0))))
      (concord-object-put fanqie-cobj '->final-character
			  (list (hanzi-syllable-get-fanqie-final (aref fanqie 1)))))
    fanqie-cobj))


;;;###autoload
(defun hdic-tsj-define-chars ()
  (let ((entry-buf (find-file-noselect hdic-tsj-entries-file))
	(def-buf (find-file-noselect hdic-tsj-definitions-file))
	(code 0)
	tsj-sj2-id
	new-glyph-page-id
	new-glyph-line-id
	new-glyph-cnum-id
	new-glyph-id
	tsj-gi tsj-char
	entry
	word
	word-desc
	word-remarks
	structure)
    (with-current-buffer entry-buf
      (goto-char (point-min))
      (while (re-search-forward
	      "^s[^\t]+\t\\([^\t]+\\)\t\u65B0\\([^---\t]+\\)-\\([^---\t]\\)\\([^---\t]\\)\t[^\t]*\t[^\t]*\t\\([^\t]+\\)\t"
	      nil t)
	(setq tsj-sj2-id (intern (match-string 1))
	      new-glyph-page-id (match-string 2)
	      new-glyph-line-id (match-string 3)
	      new-glyph-cnum-id (match-string 4)
	      entry (match-string 5))
	(setq word-desc nil
	      word-remarks nil)
	(with-current-buffer def-buf
	  (goto-char (point-min))
	  (when (re-search-forward (format "^%s\t\\([^\t]+\\)\t\\([^\t]*\\)\t"
					   tsj-sj2-id)
				   nil t)
	    (setq word (match-string 1)
		  word-desc (match-string 2)
		  word-remarks (buffer-substring (match-end 0)(point-at-eol)))
	    (if (string= word-desc "")
		(setq word-desc nil))
	    (if (string= word-remarks "")
		(setq word-remarks nil))))
	(setq code (1+ code))
	(when (and (file-exists-p
		    (expand-file-name
		     (concat
		      (setq new-glyph-id (format "%s-0%s0%s"
						 new-glyph-page-id
						 new-glyph-line-id
						 new-glyph-cnum-id))
		      ".jpg")
		     hdic-tsj-image-dir))
		   (setq tsj-gi
			 (define-char (list (cons '===chise-hdic-tsj code)))))
	  (put-char-attribute tsj-gi '=hdic-tsj-glyph-id new-glyph-id)
	  (put-char-attribute tsj-gi 'hdic-tsj-word-id tsj-sj2-id)
	  (put-char-attribute tsj-gi 'hdic-tsj-word word)
	  (if word-desc
	      (put-char-attribute tsj-gi 'hdic-tsj-word-description (list word-desc)))
	  (if word-remarks
	      (put-char-attribute tsj-gi 'hdic-tsj-word-remarks (list word-remarks)))
	  (if (= (length entry) 1)
	      (put-char-attribute tsj-gi '<-HDIC-TSJ (list (aref entry 0)))
	    (when (setq tsj-char (define-char (list (cons '=chise-hdic-tsj code))))
	      (put-char-attribute tsj-char '->subsumptive (list tsj-gi))
	      (if (and (setq structure (ids-parse-string entry))
		       (setq structure (cdr (assq 'ideographic-structure structure))))
		  (unless (get-char-attribute tsj-char 'ideographic-structure)
		    (put-char-attribute
		     tsj-char 'ideographic-structure structure))))))))))


(defvar hdic-syp-previous-bibliography-object nil)

(defun hdic-syp-parse-fanqie (string)
  (if (string-match "^\\(又?\\)\\(..\\)\\(切。?\\)" string)
      (list (match-string 1 string)
	    (match-string 2 string)
	    (match-string 3 string)
	    (substring string (match-end 0)))))

(defun hdic-syp-description-markup-fanqie (string &optional character)
  (let ((ret (hdic-syp-parse-fanqie string))
	prefix fanqie trailer rest
	fanqie-cobj)
    (when ret
      (setq hdic-syp-previous-bibliography-object nil)
      (setq prefix (pop ret)
	    fanqie (pop ret)
	    trailer (pop ret)
	    rest (pop ret))
      ;; (setq fanqie (format "%s%c" fanqie (aref trailer 0))
      ;;       trailer (substring trailer 1))
      (setq fanqie-cobj (hanzi-syllable-get-fanqie fanqie))
      (when character
	(concord-object-adjoin fanqie-cobj 'characters character)
	(dolist (abst-char (get-char-attribute character '<-HDIC-SYP))
	  (put-char-attribute abst-char 'sound@fanqie
			      (adjoin fanqie-cobj
				      (get-char-attribute abst-char 'sound@fanqie)))))
      (cons (list (list* 'pron '(:type fanqie)
			 (if (string= prefix "")
			     (list fanqie-cobj
				   (list 'suffix '() trailer))
			   (list (list 'prefix '() prefix)
				 fanqie-cobj
				 (list 'suffix '() trailer)))))
	    rest))))

(defun hdic-syp-parse-2fanqie (string)
  (if (string-match "^\\(..\\)\\(..\\)\\(二切。?\\)" string)
      (list (match-string 1 string)
	    (match-string 2 string)
	    (match-string 3 string)
	    (substring string (match-end 0)))))

(defun hdic-syp-description-markup-2fanqie (string &optional character)
  (let ((ret (hdic-syp-parse-2fanqie string))
	fanqie1 fanqie2 trailer rest
	fanqie1-cobj
	fanqie2-cobj)
    (when ret
      (setq hdic-syp-previous-bibliography-object nil)
      (setq fanqie1 (pop ret)
	    fanqie2 (pop ret)
	    trailer (pop ret)
	    rest (pop ret))
      (setq fanqie1-cobj (hanzi-syllable-get-fanqie fanqie1))
      (setq fanqie2-cobj (hanzi-syllable-get-fanqie fanqie2))
      (when character
	(concord-object-adjoin fanqie1-cobj 'characters character)
	(concord-object-adjoin fanqie2-cobj 'characters character)
	(dolist (abst-char (get-char-attribute character '<-HDIC-SYP))
	  (put-char-attribute abst-char 'sound@fanqie
			      (adjoin fanqie1-cobj
				      (adjoin fanqie2-cobj
					      (get-char-attribute abst-char 'sound@fanqie))))))
      (cons (list (list 'pron '(:type fanqie)
			fanqie1-cobj
			fanqie2-cobj
			(list 'suffix '() trailer)))
	    rest))))

(defun hdic-syp-parse-pron-similar (string)
  (if (string-match "^\\(又?音\\)\\(.\\)\\(。\\)" string)
      (list (match-string 1 string)
	    (match-string 2 string)
	    (match-string 3 string)
	    (substring string (match-end 0)))))

(defun hdic-syp-description-markup-pron-similar (string &optional character)
  (let ((ret (hdic-syp-parse-pron-similar string))
	prefix similar trailer rest)
    (when ret
      (setq hdic-syp-previous-bibliography-object nil)
      (setq prefix (pop ret)
	    similar (pop ret)
	    trailer (pop ret)
	    rest (pop ret))
      (cons (list (list 'pron '(:type similar)
			(list 'prefix () prefix)
			(list 'exact '() similar)
			(list 'suffix '() trailer)))
	    rest))))
      
(defun hdic-syp-parse-ditto (string)
  (if (string-match "^\\(（?[並亦]?\\)\\(同上\\|古文\\|籀文\\|俗\\)\\([。、]）?\\)" string)
      (list (match-string 1 string)
	    (match-string 2 string)
	    (match-string 3 string)
	    (substring string (match-end 0)))))

(defun hdic-syp-parse-variant1 (string)
  (if (string-match "^\\(並?\\)\\(古文\\|籀文\\)\\(、?\\)\\([^。]*\\)\\(。?\\)" string)
      (list (match-string 1 string)
	    (match-string 2 string)
	    (match-string 3 string)
	    (match-string 4 string)
	    (match-string 5 string)
	    (substring string (match-end 0)))))

(defun hdic-syp-parse-citation (string)
  (if (or (string-match "^\\(《[^。、]*》\\)\\([曰云]?：\\)\\([^。]*\\)\\(。?\\)" string)
	  (string-match "^\\([^。、]*\\)\\([曰云]：\\)\\([^。]*\\)\\(。?\\)" string))
      (list (match-string 1 string)
	    (match-string 2 string)
	    (substring string (match-beginning 3)(match-end 4))
	    (substring string (match-end 0)))))

(defun hdic-syp-description-markup-citation (string &optional character)
  (let ((ret (hdic-syp-parse-citation string))
	bib-name iwaku text rest
	bib-name-main
	bib-cobj abst-char sw-char
	creator-cobj)
    (when ret
      (setq bib-name (pop ret)
	    iwaku (pop ret)
	    text (pop ret)
	    rest (pop ret))
      (if (string-match "《\\(.+\\)》" bib-name)
	  (setq bib-name-main (match-string 1 bib-name))
	(setq bib-name-main bib-name))
      (setq bib-cobj
	    (or (concord-decode-object '=abbreviated-title bib-name-main 'bibliography)
		(concord-decode-object '=abbreviated-title@_1 bib-name-main 'bibliography)
		(concord-decode-object '=abbreviated-title@_2 bib-name-main 'bibliography)
		(concord-decode-object '=title bib-name-main 'bibliography)
		(concord-decode-object '=name bib-name-main 'creator)))
      (cond
       ((and bib-cobj
	     (eq (concord-object-genre bib-cobj) 'bibliography))
	(setq hdic-syp-previous-bibliography-object bib-cobj)
	)
       ((and bib-cobj
	     (eq (concord-object-genre bib-cobj) 'creator)
	     hdic-syp-previous-bibliography-object
	     (setq ret (find-if (lambda (cell)
				  (member hdic-syp-previous-bibliography-object
					  (concord-object-get cell '->commented)))
				(concord-object-get bib-cobj '<-author))))
	(setq creator-cobj bib-cobj
	      bib-cobj ret)
	)
       (t
	(setq hdic-syp-previous-bibliography-object nil)
	))
      (cons
       (if bib-cobj
	   (cond
	    ((and character
		  (eq (concord-object-get bib-cobj '=chise-bib-id)
		      'shuowen)
		  (setq abst-char (get-char-attribute character '<-HDIC-SYP))
		  (setq sw-char (get-char-attribute (car abst-char)
						    '->Small-Seal@shuowen)))
	     (list (list 'cite '()
			 (list 'object (list :tag 'bibl :object (car sw-char))
			       bib-name)
			 iwaku
			 (list 'sense '() text)))
	     )
	    (creator-cobj
	     (list (list 'cite '()
			 (list 'object (list :tag 'bibl
					     :object bib-cobj :creator creator-cobj)
			       bib-name)
			 iwaku
			 (list 'sense () text)))
	     )
	    (t
	     (list (list 'cite '()
			 (list 'object (list :tag 'bibl :object bib-cobj)
			       bib-name)
			 iwaku
			 (list 'sense () text)))
	     ))
	 (list (list 'cite '()
		     (list 'bibl () bib-name)
		     iwaku
		     (list 'sense () text))))
       rest))))

(defun hdic-syp-parse-def (string)
  (if (string-match "^\\(又?\\)\\([^。、《》]+\\)\\(也[。、]\\)" string)
      (list (match-string 1 string)
	    (match-string 2 string)
	    (match-string 3 string)
	    (substring string (match-end 0)))))

(defun hdic-syp-description-markup-def (string &optional character)
  (let ((ret (hdic-syp-parse-def string))
	prefix def-str suffix rest)
    (when ret
      (setq hdic-syp-previous-bibliography-object nil)
      (setq prefix (pop ret)
	    def-str (pop ret)
	    suffix (pop ret)
	    rest (pop ret))
      (cond
       ((= (length def-str) 1)
	(cons (list (list* 'relation '(:type synonym)
			   (if (string= prefix "")
			       (list (list 'target '() (aref def-str 0))
				     (list 'suffix '() suffix))
			     (list (list 'prefix '() prefix)
				   (list 'target '() (aref def-str 0))
				   (list 'suffix '() suffix)))))
	      rest))
       (t
	(cons (list (list* 'def '(:type synonym)
			   (if (string= prefix "")
			       (list (list 'target '() def-str)
				     (list 'suffix '() suffix))
			     (list (list 'prefix '() prefix)
				   (list 'target '() def-str)
				   (list 'suffix '() suffix)))))
	      rest))
       ))))

(defun hdic-syp-parse-other-description (string)
  (if (string-match "^\\([^。《》]+。?\\)" string)
      (list (match-string 1 string)
	    (substring string (match-end 0)))))

(defun hdic-syp-description-markup-other-description (string &optional character)
  (let ((ret (hdic-syp-parse-other-description string)))
    (when ret
      (setq hdic-syp-previous-bibliography-object nil)
      (cons (list (list 'sense '() (pop ret)))
	    (pop ret)))))

(defun hdic-syp-parse-2chars (string)
  (if (or (string-match "^（?\\(上：\\)\\([^。《》]+。\\)\\(下：\\)\\([^。《》]+。\\)\\([^。《》]+。\\)?）?" string)
	  (string-match "^【..】\\(上：\\)\\([^。《》]+。\\)\\(下：\\)\\([^。《》]+。\\)\\([^。《》]+。\\)?" string))
      (list (match-string 1 string)
	    (match-string 2 string)
	    (match-string 3 string)
	    (match-string 4 string)
	    (match-string 5 string)
	    (substring string (match-end 0)))))

(defun hdic-syp-description-markup-insert-char-into-prefix (prefix char)
  (if (string-match "：" prefix)
      (format "%s (%c)："
	      (substring prefix 0 (match-beginning 0))
	      char)
    prefix))

(defun hdic-syp-description-markup-2chars (string &optional character first second parsed)
  (let (ret 
	ret1 ret2 ret3
	prefix1 pron1
	prefix2 pron2
	desc rest)
    (setq ret
	  (or parsed
	      (hdic-syp-parse-2chars string)))
    (when ret
      (setq prefix1 (pop ret)
	    pron1 (pop ret)
	    prefix2 (pop ret)
	    pron2 (pop ret)
	    desc (pop ret)
	    rest (pop ret))
      (setq ret1 (hdic-syp-markup-description pron1 character)
	    ret2 (hdic-syp-markup-description pron2 character)
	    ret3 (hdic-syp-markup-description desc character))
      (cons (if (eq character first)
		(list* (list* 'this '()
			      (cons  (list 'subj '() prefix1)
				     ret1))
		       (list* 'alt '()
			      (cons  (list 'alt-subj (list :tag 're :object second)
					   (hdic-syp-description-markup-insert-char-into-prefix
					    prefix2 second))
				     ret2))
		       ret3)
	      (list* (list* 'alt '()
			    (cons  (list 'alt-subj (list :tag 're :object first)
					 (hdic-syp-description-markup-insert-char-into-prefix
					  prefix1 first))
				   ret1))
		     (list* 'this '()
			    (cons  (list 'subj '() prefix2)
				   ret2))
		     ret3))
	    rest))))

(defun hdic-syp-parse-variant2 (string)
  (when (or (string-match "^\\([^。、]+作\\)\\([^。、字也]\\)\\([字也]?[。、]\\)" string)
	    (string-match "^\\(亦作\\)\\([^。、字]+\\)\\([字]?[。、]\\)" string)
	    (string-match "^\\(古文\\|古\\|古爲\\|籀文\\|&AJ1-02831;\\)\\([^。、字]\\)\\([字]?[。、]\\)" string)
	    (string-match "^\\(古文&AJ1-01166;爲\\|&AJ1-02067;&AJ1-01166;爲\\)\\([^。、字]\\)\\(字。\\)" string)
	    (string-match "^\\(古文&AJ1-01581;字\\)\\([^。、字]\\)\\(也。\\)" string))
    (let ((type (match-string 1 string))
	  (target (match-string 2 string))
	  (trailer (match-string 3 string))
	  (rest (substring string (match-end 0)))
	  len)
      (setq len (length target))
      (if (and (> len 1)
	       (eq (aref target (1- len)) ?同))
	  (setq target (substring target 0 (1- len))
		trailer (concat "同" trailer)))
      (list type target trailer rest))))

(defun hdic-add-relation (subject rel-type target source)
  (let (rel-val
	pos rel-src-f rel-src-v)
    (when (characterp target)
      (setq rel-val (get-char-attribute subject rel-type))
      (unless (memq target rel-val)
	(put-char-attribute subject rel-type
			    (append rel-val (list target))))
      (when (setq pos (position target (get-char-attribute subject rel-type)))
	(setq rel-src-f (intern (format "%s$_%d*sources"
					rel-type (1+ pos))))
	(unless (memq source
		      (setq rel-src-v
			    (get-char-attribute subject rel-src-f)))
	  (put-char-attribute subject rel-src-f
			      (append rel-src-v (list source))))))))

(defun hdic-syp-description-markup-variant2 (string &optional character)
  (let ((ret (hdic-syp-parse-variant2 string))
	type target trailer rest
	rel-type ; rel-val
	target-list is-ret is-char
	char-rep ; pos rel-src-f rel-src-v
	sw-char type-form)
    (when ret
      (setq hdic-syp-previous-bibliography-object nil)
      (setq type (pop ret)
	    target (pop ret)
	    trailer (pop ret)
	    rest (pop ret))
      (setq rel-type
	    (cond
	     ((string= type "本作")	'->original)
	     ((string= type "本亦作")	'->original)
	     ((string= type "古作")	'->ancient)
	     ((string= type "古爲")	'->ancient)
	     ((string= type "古文作")	'->ancient)
	     ((string= type "古文")	'<-ancient)
	     ((string= type "古")	'<-ancient)
	     ((string= type "古文&AJ1-01166;爲")	'<-ancient)
	     ((string= type "籀文作")	'->Zhouwen)
	     ((string= type "籀文")	'<-Zhouwen)
	     ((string= type "篆文作")	'->Small-Seal)
	     ((string= type "&AJ1-02067;&AJ1-01166;爲")	'<-Liwen)
	     ((string= type "今作")	'->Liwen)
	     ((string= type "俗作")	'->vulgar)
	     ((string= type "俗")	'<-vulgar)
	     ((string= type "今俗作")	'->vulgar)
	     ((string= type "《說文》作") '->formed@shuowen)
	     (t				'->formed)
	     ))
      (if (and (>= (length target) 3)
	       (setq is-ret (ids-parse-string target))
	       (consp is-ret)
	       (setq is-ret (cdr (assq 'ideographic-structure is-ret))))
	  (if (setq is-char (car (ideographic-structure-find-chars is-ret)))
	      (setq target-list (list is-char))
	    (setq target-list (list target)))
	(setq target-list (string-to-list target)))
      (setq type-form
	    (if rel-type
		(list 'predicate
		      (list :feature rel-type)
		      type)
	      type))
      (when character
	(setq char-rep
	      (or (and (setq char-rep (get-char-attribute character '<-HDIC-SYP))
		       (car char-rep))
		  character))
	(when (and (eq rel-type '->formed@shuowen)
		   (setq sw-char (get-char-attribute char-rep '->Small-Seal@shuowen))
		   (null (cdr sw-char))
		   (setq sw-char (car sw-char)))
	  (setq type-form
		(list 'predicate
		      (list :feature rel-type)
		      (list 'object
			    (list :object sw-char)
			    "《說文》")
		      "作")))
	(dolist (tc target-list)
	  (hdic-add-relation char-rep rel-type tc 'yupian@hdic-syp)
          ;; (when (characterp tc)
          ;;   (setq rel-val (get-char-attribute char-rep rel-type))
          ;;   (unless (memq tc rel-val)
          ;;     (put-char-attribute char-rep
          ;;                         rel-type
          ;;                         (append rel-val (list tc))))
          ;;   (when (setq pos (position tc (get-char-attribute char-rep rel-type)))
          ;;     (setq rel-src-f (intern (format "%s$_%d*sources"
          ;;                                     rel-type (1+ pos))))
          ;;     (unless (memq 'yupian@hdic-syp
          ;;                   (setq rel-src-v
          ;;                         (get-char-attribute char-rep rel-src-f)))
          ;;       (put-char-attribute char-rep rel-src-f
          ;;                           (append rel-src-v '(yupian@hdic-syp))))))
	  ))
      (cons (list (list* 'relation '(:type variant)
			 type-form
			 (list* 'target '() target-list)
			 (if trailer
			     (list (list 'suffix '() trailer)))))
	    rest))))

(defun hdic-syp-markup-description (string &optional character)
  (let ((rest string)
	dest ret)
    (while (and (not (string= rest ""))
		(setq ret
		      (or (hdic-syp-description-markup-fanqie rest character)
			  (hdic-syp-description-markup-2fanqie rest character)
			  (hdic-syp-description-markup-pron-similar rest character)
			  (hdic-syp-description-markup-citation rest character)
			  (hdic-syp-description-markup-variant2 rest character)
			  (hdic-syp-description-markup-def rest character)
			  (hdic-syp-description-markup-other-description rest character))))
      (setq dest (nconc dest (car ret))
	    rest (cdr ret)))
    (if (string= rest "")
	dest
      (nconc dest (list rest)))))

;;;###autoload
(defun hdic-syp-define-chars ()
  (let ((entry-buf (find-file-noselect hdic-syp-entries-file))
	(code 0)
	syp-id
	syp-def syp-def-list syp-def-list0
	syp-gi syp-char syp-gi0
	entry
	entry-char entry-char0
	entry-original
	entry-original-char
	structure structure-original
	ret rel-fn
	ret2 multi-entry)
    (with-current-buffer entry-buf
      (goto-char (point-min))
      (while (re-search-forward
	      "^\\([a-z][0-9][0-9][0-9][^\t]+\\)\t[^\t]+\t[^\t]+\t\\([^\t]*\\)\t\\([^\t]*\\)\t\\([^\t]*\\)\t" nil t)
	(setq syp-id (intern (match-string 1))
	      entry (match-string 2)
	      entry-original (match-string 3)
	      syp-def (match-string 4))
	(cond
	 ((setq ret (hdic-syp-parse-ditto syp-def))
	  (if (or (null syp-gi0)
		  (string= (car ret) ""))
	      (setq syp-gi0 syp-gi
		    entry-char0 entry-char))
	  )
	 ((setq ret2 (hdic-syp-parse-2chars syp-def))
	  (if (null multi-entry)
	      (setq multi-entry 0)
	    (setq multi-entry (1+ multi-entry)
		  syp-gi0 syp-gi
		  entry-char0 entry-char))
	  )
	 (t
	  (setq syp-gi0 nil
		entry-char0 nil
		multi-entry nil)
	  ))
	(setq code (1+ code))
	(when (and (file-exists-p
		    (expand-file-name (format "%s.jpg" syp-id)
				      hdic-syp-image-dir))
		   (setq syp-gi
			 (define-char (list (cons '===chise-hdic-syp code)))))
	  (put-char-attribute syp-gi '=hdic-syp-entry-id syp-id)
          ;; (put-char-attribute syp-gi 'hdic-syp-description (list syp-def))
	  (setq structure nil)
	  (setq entry-char
		(if (> (length entry) 0)
		    (if (= (length entry) 1)
			(aref entry 0)
		      (if (setq structure
				(cdr (assq 'ideographic-structure
					   (ids-parse-string entry))))
			  (car (ideographic-structure-find-chars structure))))))
	  (cond
	   (entry-char
	    (put-char-attribute syp-gi '<-HDIC-SYP (list entry-char))
	    )
	   ((setq syp-char (define-char (list (cons '=chise-hdic-syp code))))
	    (put-char-attribute syp-char '->subsumptive (list syp-gi))
	    (if structure
		(put-char-attribute
		 syp-char 'ideographic-structure structure))
	    (setq entry-char syp-char)
	    )
	   (t
	    (setq entry-char syp-gi)
	    ))
	  (cond 
	   (ret
	    (setq rel-fn
		  (cdr (assoc (nth 1 ret)
			      '(("同上" . <-same)
				("古文" . <-ancient)
				("籀文" . <-Zhouwen)
				("俗"	. <-vulgar)
				))))
	    (setq syp-def-list
		  (cons (list* 'relation
			       (list* :type 'variant
				      (if rel-fn
					  (list :feature rel-fn)))
			       (nconc
				(unless (string= (car ret) "")
				  (list (list 'prefix '() (car ret))))
				(list (list 'object
					    (list :object syp-gi0)
					    (nth 1 ret)))
				(if (string= (nth 2 ret) "")
				    nil
				  (list (list 'suffix '() (nth 2 ret))))))
			(hdic-syp-markup-description (nth 3 ret) syp-gi)))
	    (when rel-fn
	      (hdic-add-relation entry-char rel-fn entry-char0 'yupian@hdic-syp)
              (hdic-add-relation syp-gi rel-fn syp-gi0 'yupian@hdic-syp)
	      )
	    )
	   (ret2
	    (if (null syp-gi0)
		(setq syp-def-list nil)
	      (setq syp-def-list0 (car (hdic-syp-description-markup-2chars syp-def syp-gi0
									   syp-gi0 syp-gi
									   ret2)))
	      (put-char-attribute syp-gi0 'hdic-syp-description syp-def-list0)
	      (setq syp-def-list (car (hdic-syp-description-markup-2chars syp-def syp-gi
									  syp-gi0 syp-gi
									  ret2))))
	    )
	   (t
	    (setq syp-def-list (hdic-syp-markup-description syp-def syp-gi))
	    ))
	  (put-char-attribute syp-gi 'hdic-syp-description
			      (if syp-def-list
				  syp-def-list
				(list syp-def)))
	  (setq structure-original nil)
	  (when (> (length entry-original) 0)
	    (setq entry-original-char
		  (if (= (length entry-original) 1)
		      (aref entry-original 0)
		    (if (setq structure-original
			      (cdr (assq 'ideographic-structure
					 (ids-parse-string entry-original))))
			(car (ideographic-structure-find-chars structure-original)))))
	    (cond
	     (entry-original-char
	      (put-char-attribute syp-gi '<-HDIC-SYP
				  (cons entry-original-char
					(get-char-attribute syp-gi '<-HDIC-SYP)))
	      )
	     (t
	      (when (setq syp-char (define-char (list (cons '=chise-hdic-syp code))))
		(put-char-attribute syp-char '->subsumptive (list syp-gi))
		(if structure-original
		    (put-char-attribute
		     syp-char 'ideographic-structure structure-original)))
	      )))
	  (when noninteractive
	    (princ (format "HDIC-SYP-%s (%05d) (%c (corresponding to %c)) is defined.\n"
			   syp-id code syp-gi entry-char)))
	  )))))

;;;###autoload
(defun hdic-syp-find-char-by-id (syp-id)
  (map-char-attribute (lambda (c v)
			(if (eq (get-char-attribute c '=hdic-syp-entry-id)
				syp-id)
			    c))
		      '===chise-hdic-syp))


;;;###autoload
(defun hdic-ktb-define-chars ()
  (let ((entry-buf (find-file-noselect hdic-ktb-entries-file))
	(ndl-buf (find-file-noselect hdic-ktb-ndl-file))
	(code 0)
	ktb-id
	ktb-page
	ktb-type
	ktb-diff
	ktb-def
	syid yyid
	ktb-gi ktb-char
	ndl
	entry
	entry-char
	structure ret)
    (with-current-buffer entry-buf
      (goto-char (point-min))
      (while (re-search-forward
	      "^\\([0-9]_[^\t]+\\)\t[^\t]*\t[^\t]*\t\\([^\t]*\\)\t\\([^\t]*\\)\t\\([^\t]*\\)\t\\([^\t]*\\)\t\\([^\t]*\\)\t\\([^\t]*\\)\t" nil t)
	(setq ktb-id (match-string 1)
	      entry (match-string 2)
	      ktb-type (match-string 3)
	      ktb-diff (match-string 4)
	      ktb-def (match-string 5)
	      syid (match-string 6)
	      yyid (match-string 7))
	(setq ktb-page (if (string-match "[0-9][0-9]$" ktb-id)
			   (substring ktb-id 0 (match-beginning 0))
			 ktb-id)
	      ktb-id (intern ktb-id))
	(setq code (1+ code))
	(when (and (file-exists-p
		    (expand-file-name (format "%s.jpg" ktb-id)
				      hdic-ktb-image-dir))
		   (setq ktb-gi
			 (define-char (list (cons '===chise-hdic-ktb code)))))
	  (put-char-attribute ktb-gi '=hdic-ktb-entry-id ktb-id)
	  (setq ndl (with-current-buffer ndl-buf
		      (goto-char (point-min))
		      (if (re-search-forward
			   (format "^v[^\t]+\t[^\t]+\t[^\t]+\t%s\t\\([^\t]+\\)$"
				   ktb-page)
			   nil t)
			  (match-string 1))))
	  (if (and ndl
		   (string-match "info:ndljp/pid/" ndl))
	      (put-char-attribute ktb-gi 'hdic-ktb-ndl-pid
				  (intern (substring ndl (match-end 0)))))

	  (unless (string= ktb-type "")
	    (setq ktb-type (intern ktb-type))
	    (put-char-attribute ktb-gi 'hdic-ktb-entry-type ktb-type))
	  (unless (string= ktb-diff "")
	    (put-char-attribute ktb-gi 'hdic-ktb-diff (list ktb-diff)))
	  (unless (string= syid "")
	    (setq syid (intern syid))
            ;; (put-char-attribute ktb-gi 'hdic-ktb-syp-id syid)
	    (if (setq ret (hdic-syp-find-char-by-id syid))
		(put-char-attribute ktb-gi '->HDIC-SYP@tenrei-bansho-meigi (list ret))
	      (put-char-attribute ktb-gi 'hdic-ktb-syp-id syid))
	    )
	  (unless (string= yyid "")
	    (setq yyid (intern yyid))
	    (put-char-attribute ktb-gi 'hdic-ktb-yy-id yyid))
	  (unless (string= ktb-def "")
	    (put-char-attribute ktb-gi 'hdic-ktb-description (list ktb-def)))
	  (setq structure nil)
	  (setq entry-char
		(if (> (length entry) 0)
		    (if (= (length entry) 1)
			(aref entry 0)
		      (setq structure (cdr (assq 'ideographic-structure
						 (ids-parse-string entry))))
		      (if structure
			  (car (ideographic-structure-find-chars structure))))))
	  (if entry-char
	      (put-char-attribute ktb-gi '<-HDIC-KTB (list entry-char))
	    (when (setq ktb-char (define-char (list (cons '=chise-hdic-ktb code))))
	      (put-char-attribute ktb-char '->subsumptive (list ktb-gi))
	      (if structure
		  (put-char-attribute
		   ktb-char 'ideographic-structure structure)))))))))

;;;###autoload
(defun hdic-ktb-find-char-by-id (ktb-id)
  (map-char-attribute (lambda (c v)
			(if (eq (get-char-attribute c '=hdic-ktb-entry-id)
				ktb-id)
			    c))
		      '===chise-hdic-ktb))


;;;###autoload
(defun hdic-ktb-seal-define-chars ()
  (let ((entry-buf (find-file-noselect hdic-ktb-entries-file))
	(directory (car (directory-files "~/projects/hdic/" t
					 "Tensho head word img .*20220525$")))
	code
	glyph-code
	tb-id tb-char
	rad-char-num
	rad-dir-full
	rad-a rad-b
	rad0 rad
	entry
	entry-char
	structure)
    (dolist (rad-dir (sort
		      (directory-files directory nil "^[0-9]" t)
		      (lambda (a b)
			(setq rad-a
			      (if (string-match "^\\([0-9]+\\)" a)
				  (string-to-number (match-string 1 a))))
			(setq rad-b
			      (if (string-match "^\\([0-9]+\\)" b)
				  (string-to-number (match-string 1 b))))
			(if rad-a
			    (if rad-b
				(if (= rad-a rad-b)
				    (< (length a)(length b))
				  (< rad-a rad-b))
			      t)
			  rad-b))))
      (setq rad
	    (if (string-match "^\\([0-9]+\\)" rad-dir)
		(string-to-number (match-string 1 rad-dir))))
      (if (eq rad rad0)
	  (if rad-char-num
	      (if (< rad-char-num 128)
		  (setq rad-char-num 128)
		(setq rad-char-num (1+ rad-char-num)))
	    (setq rad-char-num 128))
	(setq rad-char-num 0))
      (setq rad-dir-full (expand-file-name rad-dir directory))
      (dolist (glyph-file (directory-files rad-dir-full nil "^T"))
	(setq code (+ (lsh rad 8) rad-char-num))
	(setq glyph-code (file-name-sans-extension glyph-file))
	(setq tb-id (substring glyph-code 1 10))
	(with-current-buffer entry-buf
	  (goto-char (point-min))
	  (when (re-search-forward (format "^%s\t[^\t]*\t[^\t]*\t\\([^\t]*\\)\t" tb-id)
				   nil t)
	    (setq entry (match-string 1))
	    (setq entry-char
		  (if (> (length entry) 0)
		      (if (= (length entry) 1)
			  (aref entry 0)
			(setq structure (cdr (assq 'ideographic-structure
						   (ids-parse-string entry))))
			(if structure
			    (car (ideographic-structure-find-chars structure))))))
	    (setq tb-char (hdic-ktb-find-char-by-id (intern tb-id)))
	    (define-char (list (cons '===chise-hdic-ktb-seal code)
			       (cons '=hdic-ktb-seal-glyph-id (intern glyph-code))
			       (if (or entry-char tb-char)
				   (cons '<-Small-Seal@tenrei-bansho-meigi
					 (if entry-char
					     (cons entry-char
						   (if tb-char
						       (list tb-char)))
					   (list tb-char))))))))
	(setq rad-char-num (1+ rad-char-num)))
      (setq rad0 rad))
    ))


;;; @ End.
;;;

(provide 'hdic-read)

;;; hdic-read.el ends here
