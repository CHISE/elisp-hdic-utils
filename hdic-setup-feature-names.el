;; -*- coding: utf-8-mcs-er; -*-

(put-char-feature-property '->HDIC-TSJ 'name@ja
			   "→ [HDIC] 新撰字鏡")
(put-char-feature-property '->HDIC-TSJ 'value-presentation-format
			   'hdic-tsj-entry-character-list)

(put-char-feature-property 'hdic-tsj-word-id 'name@ja
			   "[HDIC] 新撰字鏡 項目 ID")
(put-char-feature-property 'hdic-tsj-word-id 'value-presentation-format
			   '((link (:ref ("https://viewer.hdic.jp/tsj2/" (string)))
				   (string)) ""))

(put-char-feature-property '=hdic-tsj-glyph-id 'name@ja
			   "[HDIC] 新撰字鏡 glyph ID")


(put-char-feature-property '->HDIC-SYP 'name@ja
			   "→ [HDIC] 宋本玉篇")
(put-char-feature-property '->HDIC-SYP 'value-presentation-format
			   'entry-character-list)

(put-char-feature-property '=hdic-syp-entry-id 'name@ja
			   "[HDIC] 宋本玉篇 項目 ID")
(put-char-feature-property '=hdic-syp-entry-id 'value-presentation-format
			   '((link (:ref ("https://viewer.hdic.jp/syp/" (string)))
				   (string)) ""))

(put-char-feature-property 'hdic-syp-description 'name@ja
			   "[HDIC] 宋本玉篇 注文")
(put-char-feature-property 'hdic-syp-description 'value-presentation-format 'wiki-text)


(put-char-feature-property '->HDIC-KTB 'name@ja
			   "→ [HDIC] 篆隸萬象名義")
(put-char-feature-property '->HDIC-KTB 'value-presentation-format
			   'entry-character-list)
(put-char-feature-property '<-HDIC-SYP@tenrei-bansho-meigi 'name@ja
			   "→ [HDIC] 篆隸萬象名義")
(put-char-feature-property '<-HDIC-SYP@tenrei-bansho-meigi 'value-presentation-format
			   'entry-character-list)
(put-char-feature-property '->HDIC-SYP@tenrei-bansho-meigi 'name@ja
			   "→ [HDIC] 宋本玉篇")
(put-char-feature-property '<-HDIC-KTB 'name@ja "UCS 抽象文字")

(put-char-feature-property '=hdic-ktb-entry-id 'name@ja
			   "[HDIC] 篆隸萬象名義 項目 ID")

(put-char-feature-property 'hdic-ktb-entry-type 'name@ja
			   "[HDIC] 篆隸萬象名義 掲出字分類")
(put-char-feature-property 'hdic-ktb-description 'name@ja
			   "[HDIC] 篆隸萬象名義 注文")
(put-char-feature-property 'hdic-ktb-yy-id 'name@ja
			   "[HDIC] 対応する原本玉篇")
(put-char-feature-property 'hdic-ktb-yy-id 'value-presentation-format
			   'hdic-yy-readable)
(put-char-feature-property 'hdic-ktb-ndl-pid 'name@ja
			   "[HDIC] 篆隸萬象名義 全文画像")
(put-char-feature-property 'hdic-ktb-ndl-pid 'value-presentation-format
			   '(" "
			     (link (:ref ("https://dl.ndl.go.jp/info:ndljp/pid/" (string)))
				   "[国会図書館デジタルコレクション]")
			     " "
			     (link (:ref ((tify-url-for-ndl))) "[TIFY]")))

(put-char-feature-property '=hdic-ktb-seal-glyph-id 'name@ja
			   "[HDIC] 篆隸萬象名義 篆書掲出字 ID")
(put-char-feature-property '->Small-Seal@tenrei-bansho-meigi 'name@ja
			   "→ [HDIC] 篆隸萬象名義 篆書掲出字")
(put-char-feature-property '<-Small-Seal@tenrei-bansho-meigi 'name@ja
			   "→ [HDIC] 篆隸萬象名義 隷書掲出字")
(put-char-feature-property '<-Small-Seal@tenrei-bansho-meigi 'value-presentation-format
			   'entry-character-list)
